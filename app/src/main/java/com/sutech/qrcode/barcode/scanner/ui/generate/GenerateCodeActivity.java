package com.sutech.qrcode.barcode.scanner.ui.generate;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.sutech.qrcode.barcode.scanner.R;
import com.sutech.qrcode.barcode.scanner.databinding.ActivityGenerateCodeBinding;
import com.sutech.qrcode.barcode.scanner.helpers.constant.IntentKey;
import com.sutech.qrcode.barcode.scanner.helpers.model.Code;
import com.sutech.qrcode.barcode.scanner.ui.generatedcode.GeneratedCodeActivity;

public class GenerateCodeActivity extends AppCompatActivity implements View.OnClickListener{

    ActivityGenerateCodeBinding mBinding;
    private InterstitialAd mInterstitialAd;
    int type = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_generate_code);


//        initializeAd();

        setListeners();

        mBinding.imageViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        String tittle = getIntent().getStringExtra("tittle");
        type = getIntent().getIntExtra("type", 0);
        mBinding.textViewTittle.setText(tittle);

        mBinding.editTextContent.setHint("Enter " + tittle + " here");

    }

    @Override
    public void onResume() {
        super.onResume();
//        mInterstitialAd.loadAd(new AdRequest.Builder().build());
    }


    private void initializeAd() {
        if (this == null) {
            return;
        }

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.admob_test_interstitial_ad_unit_id));
    }

    private void setListeners() {

        mBinding.linearGenerate.setOnClickListener(this);

//        mInterstitialAd.setAdListener(new AdListener() {
//            @Override
//            public void onAdLoaded() {
//                // Code to be executed when an ad finishes loading.
//            }
//
//            @Override
//            public void onAdFailedToLoad(int errorCode) {
//                // Code to be executed when an ad request fails.
//            }
//
//            @Override
//            public void onAdOpened() {
//                // Code to be executed when the ad is displayed.
//            }
//
//            @Override
//            public void onAdLeftApplication() {
//                // Code to be executed when the user has left the app.
//            }
//
//            @Override
//            public void onAdClosed() {
//                // Code to be executed when when the interstitial ad is closed.
//                generateCode();
//            }
//        });
    }

//    private void initializeCodeTypesSpinner() {
//        ArrayAdapter arrayAdapter = ArrayAdapter.createFromResource(this,
//                R.array.code_types, android.R.layout.simple_spinner_item);
//        arrayAdapter.setDropDownViewResource(R.layout.item_spinner);
//        mBinding.spinnerTypes.setAdapter(arrayAdapter);
//    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.linearGenerate:
//                if (mInterstitialAd.isLoaded()) {
//                    mInterstitialAd.show();
//                } else {
//                    generateCode();
//                }
                generateCode();
                break;

            default:
                break;
        }
    }

    private void generateCode() {
        Intent intent = new Intent(this, GeneratedCodeActivity.class);
        if (mBinding.editTextContent.getText() != null) {
            String content = mBinding.editTextContent.getText().toString().trim();
//            int type = mBinding.spinnerTypes.getSelectedItemPosition();

            if (!TextUtils.isEmpty(content) && type != 0) {

                boolean isValid = true;

                switch (type) {
                    case Code.BAR_CODE:
                        if (content.length() > 80) {
                            Toast.makeText(this,
                                    getString(R.string.error_barcode_content_limit),
                                    Toast.LENGTH_SHORT).show();
                            isValid = false;
                        }
                        break;

                    case Code.QR_CODE:
                        if (content.length() > 1000) {
                            Toast.makeText(this,
                                    getString(R.string.error_qrcode_content_limit),
                                    Toast.LENGTH_SHORT).show();
                            isValid = false;
                        }
                        break;

                    default:
                        isValid = false;
                        break;
                }

                if (isValid) {
                    Code code = new Code(content, type);
                    intent.putExtra(IntentKey.MODEL, code);
                    startActivity(intent);
                }
            } else {
                Toast.makeText(this,
                        getString(R.string.error_provide_proper_content_and_type),
                        Toast.LENGTH_SHORT).show();
            }
        }
    }
}
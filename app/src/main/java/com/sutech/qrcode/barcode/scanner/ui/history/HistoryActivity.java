package com.sutech.qrcode.barcode.scanner.ui.history;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.sutech.qrcode.barcode.scanner.R;
import com.sutech.qrcode.barcode.scanner.databinding.ActivityHistoryBinding;
import com.sutech.qrcode.barcode.scanner.databinding.ActivitySettingsBinding;
import com.sutech.qrcode.barcode.scanner.databinding.FragmentHistoryBinding;
import com.sutech.qrcode.barcode.scanner.helpers.constant.IntentKey;
import com.sutech.qrcode.barcode.scanner.helpers.constant.PreferenceKey;
import com.sutech.qrcode.barcode.scanner.helpers.itemtouch.OnStartDragListener;
import com.sutech.qrcode.barcode.scanner.helpers.itemtouch.SimpleItemTouchHelperCallback;
import com.sutech.qrcode.barcode.scanner.helpers.model.Code;
import com.sutech.qrcode.barcode.scanner.helpers.util.ProgressDialogUtil;
import com.sutech.qrcode.barcode.scanner.helpers.util.SharedPrefUtil;
import com.sutech.qrcode.barcode.scanner.helpers.util.database.DatabaseUtil;
import com.sutech.qrcode.barcode.scanner.ui.about_us.AboutUsActivity;
import com.sutech.qrcode.barcode.scanner.ui.base.ItemClickListener;
import com.sutech.qrcode.barcode.scanner.ui.privacy_policy.PrivayPolicyActivity;
import com.sutech.qrcode.barcode.scanner.ui.scanresult.ScanResultActivity;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class HistoryActivity extends AppCompatActivity implements OnStartDragListener, ItemClickListener<Code> {

    private ActivityHistoryBinding mBinding;
    private CompositeDisposable mCompositeDisposable;
    private ItemTouchHelper mItemTouchHelper;
    private HistoryAdapter mAdapter;

    private FirebaseAnalytics firebaseAnalytics;
    Bundle bundle = new Bundle();

    private CompositeDisposable getCompositeDisposable() {
        return mCompositeDisposable;
    }

    private void setCompositeDisposable(CompositeDisposable compositeDisposable) {
        mCompositeDisposable = compositeDisposable;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_history);

        initializeToolbar();

        firebaseAnalytics = FirebaseAnalytics.getInstance(this);

        setCompositeDisposable(new CompositeDisposable());

        ProgressDialogUtil.on().showProgressDialog(this);
        getCompositeDisposable().add(DatabaseUtil.on().getAllCodes()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(codeList -> {
                    if (codeList.isEmpty()) {
                        mBinding.imageViewEmptyBox.setVisibility(View.VISIBLE);
                        mBinding.textViewNoItemPlaceholder.setVisibility(View.VISIBLE);
                    } else {
                        mBinding.textViewNoItemPlaceholder.setVisibility(View.GONE);
                        mBinding.imageViewEmptyBox.setVisibility(View.INVISIBLE);
                    }

                    getAdapter().clear();
                    getAdapter().addItem(codeList);
                    ProgressDialogUtil.on().hideProgressDialog();
                }, e -> ProgressDialogUtil.on().hideProgressDialog()));

        mBinding.recyclerViewHistory.setLayoutManager(new LinearLayoutManager(this));
        mBinding.recyclerViewHistory.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new HistoryAdapter(this);
        mBinding.recyclerViewHistory.setAdapter(mAdapter);
        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(mAdapter);
        mItemTouchHelper = new ItemTouchHelper(callback);
        mItemTouchHelper.attachToRecyclerView(mBinding.recyclerViewHistory);

        bundle = new Bundle();
        bundle.putString("ScanHistoryScr_Show", "Hiển thị màn hình Scan History");
        firebaseAnalytics.logEvent("ScanHistoryScr_Show", bundle);

//        loadSettings();
//        setListeners();
    }

    private void initializeToolbar() {
        setSupportActionBar(mBinding.toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();

                bundle = new Bundle();
                bundle.putString("ScanhistoryScr_IconBack_Click", "Click icon Back");
                firebaseAnalytics.logEvent("ScanhistoryScr_IconBack_Click", bundle);

                break;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    private HistoryAdapter getAdapter() {
        return (HistoryAdapter) mBinding.recyclerViewHistory.getAdapter();
    }

    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        mItemTouchHelper.startDrag(viewHolder);
    }

    @Override
    public void onItemClick(View view, Code item, int position) {
        Intent intent = new Intent(this, ScanResultActivity.class);
        intent.putExtra(IntentKey.MODEL, item);
        intent.putExtra(IntentKey.IS_HISTORY, true);
        startActivity(intent);
    }
}

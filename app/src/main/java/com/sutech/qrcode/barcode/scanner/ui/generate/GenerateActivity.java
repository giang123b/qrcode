package com.sutech.qrcode.barcode.scanner.ui.generate;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.sutech.qrcode.barcode.scanner.R;
import com.sutech.qrcode.barcode.scanner.databinding.ActivityGenerateBinding;
import com.sutech.qrcode.barcode.scanner.databinding.ActivityGenerateCodeBinding;
import com.sutech.qrcode.barcode.scanner.databinding.FragmentGenerateBinding;
import com.sutech.qrcode.barcode.scanner.helpers.model.Code;

public class GenerateActivity extends AppCompatActivity {

    ActivityGenerateBinding mBinding;

    private FirebaseAnalytics firebaseAnalytics;
    Bundle bundle = new Bundle();

    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        firebaseAnalytics = FirebaseAnalytics.getInstance(this);

        bundle = new Bundle();
        bundle.putString("GenerateScr_Show", "Hiển thị màn hình Generate");
        firebaseAnalytics.logEvent("GenerateScr_Show", bundle);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_generate);

        mBinding.linearLayoutText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GenerateActivity.this, GenerateCodeActivity.class);
                intent.putExtra("tittle", "Text");
                intent.putExtra("type", Code.QR_CODE);
                startActivity(intent);

                bundle = new Bundle();
                bundle.putString("GenerateScr_IconText_Click", "Click icon Text");
                firebaseAnalytics.logEvent("GenerateScr_IconText_Click", bundle);
            }
        });

        mBinding.linearLayoutURL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GenerateActivity.this, GenerateCodeActivity.class);
                intent.putExtra("tittle", "URL");
                intent.putExtra("type", Code.QR_CODE);
                startActivity(intent);

                bundle = new Bundle();
                bundle.putString("GenerateScr_IconUrl_Click", "Click icon Url");
                firebaseAnalytics.logEvent("GenerateScr_IconUrl_Click", bundle);
            }
        });

        mBinding.linearLayoutPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GenerateActivity.this, GenerateCodeActivity.class);
                intent.putExtra("tittle", "Phone");
                intent.putExtra("type", Code.QR_CODE);
                startActivity(intent);

                bundle = new Bundle();
                bundle.putString("GenerateScr_IconPhone_Click", "Click icon Phone");
                firebaseAnalytics.logEvent("GenerateScr_IconPhone_Click", bundle);
            }
        });

        mBinding.linearLayoutEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GenerateActivity.this, GenerateCodeActivity.class);
                intent.putExtra("tittle", "Email");
                intent.putExtra("type", Code.QR_CODE);
                startActivity(intent);

                bundle = new Bundle();
                bundle.putString("GenerateScr_IconEmail_Click", "Click icon Email");
                firebaseAnalytics.logEvent("GenerateScr_IconEmail_Click", bundle);
            }
        });

        mBinding.linearLayoutBarcode93.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GenerateActivity.this, GenerateCodeActivity.class);
                intent.putExtra("tittle", "Barcode-93");
                intent.putExtra("type", Code.BAR_CODE);
                startActivity(intent);

                bundle = new Bundle();
                bundle.putString("GenerateScr_Barcode93_Click", "Click icon Barcode-93");
                firebaseAnalytics.logEvent("GenerateScr_Barcode93_Click", bundle);
            }
        });

        mBinding.linearLayoutBarcode39.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GenerateActivity.this, GenerateCodeActivity.class);
                intent.putExtra("tittle", "Barcode-39");
                intent.putExtra("type", Code.BAR_CODE);
                startActivity(intent);

                bundle = new Bundle();
                bundle.putString("GenerateScr_Barcode39_Click", "Click icon Barcode-39");
                firebaseAnalytics.logEvent("GenerateScr_Barcode39_Click", bundle);
            }
        });

        mBinding.linearLayoutBarcodePDF417.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GenerateActivity.this, GenerateCodeActivity.class);
                intent.putExtra("tittle", "PDF 417");
                intent.putExtra("type", Code.BAR_CODE);
                startActivity(intent);

                bundle = new Bundle();
                bundle.putString("GenerateScr_IconPdf417_Click", "Click icon Pdf 417");
                firebaseAnalytics.logEvent("GenerateScr_IconPdf417_Click", bundle);
            }
        });

        mBinding.linearLayoutBarcodeAZTEC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GenerateActivity.this, GenerateCodeActivity.class);
                intent.putExtra("tittle", "AZTEC");
                intent.putExtra("type", Code.BAR_CODE);
                startActivity(intent);

                bundle = new Bundle();
                bundle.putString("GenerateScr_IconAztec_Click", "Click icon Aztec");
                firebaseAnalytics.logEvent("GenerateScr_IconAztec_Click", bundle);
            }
        });

        initializeToolbar();
    }

    private void initializeToolbar() {
        setSupportActionBar(mBinding.toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();

                bundle = new Bundle();
                bundle.putString("SettingScr_IconPolicy_Click", "Click");
                firebaseAnalytics.logEvent("SettingScr_IconPolicy_Click", bundle);
                break;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
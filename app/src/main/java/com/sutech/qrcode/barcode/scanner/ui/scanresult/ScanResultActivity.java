package com.sutech.qrcode.barcode.scanner.ui.scanresult;

import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.ads.formats.MediaView;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.google.android.gms.ads.nativead.NativeAd;
//import com.google.android.gms.ads.nativead.NativeAdView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.sutech.qrcode.barcode.scanner.R;
import com.sutech.qrcode.barcode.scanner.databinding.ActivityScanResultBinding;
import com.sutech.qrcode.barcode.scanner.helpers.constant.IntentKey;
import com.sutech.qrcode.barcode.scanner.helpers.constant.PreferenceKey;
import com.sutech.qrcode.barcode.scanner.helpers.model.Code;
import com.sutech.qrcode.barcode.scanner.helpers.util.SharedPrefUtil;
import com.sutech.qrcode.barcode.scanner.helpers.util.TimeUtil;
import com.sutech.qrcode.barcode.scanner.helpers.util.database.DatabaseUtil;
import com.sutech.qrcode.barcode.scanner.ui.settings.SettingsActivity;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Locale;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.schedulers.Schedulers;

public class ScanResultActivity extends AppCompatActivity implements View.OnClickListener {

    private CompositeDisposable mCompositeDisposable;
    private ActivityScanResultBinding mBinding;
    private Menu mToolbarMenu;
    private Code mCurrentCode;
    private boolean mIsHistory, mIsPickedFromGallery;

    private FirebaseAnalytics firebaseAnalytics;
    Bundle bundle = new Bundle();

    private UnifiedNativeAd nativeAd;

    public CompositeDisposable getCompositeDisposable() {
        return mCompositeDisposable;
    }

    public void setCompositeDisposable(CompositeDisposable compositeDisposable) {
        mCompositeDisposable = compositeDisposable;
    }

    public Code getCurrentCode() {
        return mCurrentCode;
    }

    public void setCurrentCode(Code currentCode) {
        mCurrentCode = currentCode;
    }

    public Menu getToolbarMenu() {
        return mToolbarMenu;
    }

    public void setToolbarMenu(Menu toolbarMenu) {
        mToolbarMenu = toolbarMenu;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_scan_result);
        setCompositeDisposable(new CompositeDisposable());
//        playAd();
        getWindow().setBackgroundDrawable(null);
        loadQRCode();
        setListeners();
        checkInternetConnection();

        ImageView imageViewBack = findViewById(R.id.imageViewBack);

        imageViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle = new Bundle();
                bundle.putString("ScanResultScr_IconBack_Click", "Click icon Back");
                firebaseAnalytics.logEvent("ScanResultScr_IconBack_Click", bundle);

                finish();
            }
        });

        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        bundle = new Bundle();
        bundle.putString("ScanResultScr_Show", "Hiện thị màn hình Scan Result");
        firebaseAnalytics.logEvent("ScanResultScr_Show", bundle);

        mBinding.imageViewStarFeefback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // get prompts.xml view
                LayoutInflater li = LayoutInflater.from(ScanResultActivity.this);
                View dialog = li.inflate(R.layout.dialog_experience, null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ScanResultActivity.this);

                // set prompts.xml to alertdialog builde
                alertDialogBuilder.setView(dialog);

                final LinearLayout linearBad = dialog
                        .findViewById(R.id.linearBad);

                final LinearLayout linearGood = dialog
                        .findViewById(R.id.linearGood);

                final LinearLayout linearExcellent = dialog
                        .findViewById(R.id.linearExcellent);

                final TextView textViewNoThanks = dialog
                        .findViewById(R.id.textViewNoThanks);

                final LinearLayout linearFeedback = dialog
                        .findViewById(R.id.linearFeedback);

                final TextView textViewShow = dialog
                        .findViewById(R.id.textViewShow);

                final LinearLayout linearButtonFeedback = dialog
                        .findViewById(R.id.linearButtonFeedback);

                final ImageView imageViewIconFeedback = dialog
                        .findViewById(R.id.imageViewIconFeedback);

                final ImageView iconBad = dialog
                        .findViewById(R.id.iconBad);

                final ImageView iconGood = dialog
                        .findViewById(R.id.iconGood);

                final ImageView iconExcellent = dialog
                        .findViewById(R.id.iconExcellent);

                final TextView textViewFeedback = dialog
                        .findViewById(R.id.textViewFeedback);

                AlertDialog alertDialog =
                        alertDialogBuilder.create();

                linearBad.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        linearFeedback.setVisibility(View.VISIBLE);
                        textViewNoThanks.setVisibility(View.GONE);
                        imageViewIconFeedback.setImageResource(R.drawable.ic_email_white);
                        textViewFeedback.setText("Feed back to us");
                        textViewShow.setText("Error Reporting \n Let us know about application.");

                        linearButtonFeedback.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(Intent.ACTION_SENDTO);
                                intent.putExtra(Intent.EXTRA_SUBJECT, "Feedback App QR & Barcode Scanner");
                                intent.putExtra(Intent.EXTRA_TEXT, "What is your problem?");
                                intent.setData(Uri.parse("mailto:sutechmobile@gmail.com")); // or just "mailto:" for blank
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
                                startActivity(intent);
                            }
                        });

                        iconBad.setImageResource(R.mipmap.ic_selected_bad);
                        iconGood.setImageResource(R.mipmap.ic_good);
                        iconExcellent.setImageResource(R.mipmap.ic_excilent);
                    }
                });

                linearGood.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        linearFeedback.setVisibility(View.VISIBLE);
                        textViewNoThanks.setVisibility(View.GONE);
                        imageViewIconFeedback.setImageResource(R.drawable.ic_email_white);
                        textViewFeedback.setText("Mail to us");
                        textViewShow.setText("Give Suggestions \n Contribute to improve the application.");

                        linearButtonFeedback.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(Intent.ACTION_SENDTO);
                                intent.putExtra(Intent.EXTRA_SUBJECT, "Feedback App QR & Barcode Scanner");
                                intent.putExtra(Intent.EXTRA_TEXT, "What is your problem?");
                                intent.setData(Uri.parse("mailto:sutechmobile@gmail.com")); // or just "mailto:" for blank
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
                                startActivity(intent);
                            }
                        });

                        iconBad.setImageResource(R.mipmap.ic_bad);
                        iconGood.setImageResource(R.mipmap.ic_selected_good);
                        iconExcellent.setImageResource(R.mipmap.ic_excilent);
                    }
                });

                linearExcellent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        linearFeedback.setVisibility(View.VISIBLE);
                        textViewNoThanks.setVisibility(View.GONE);
                        imageViewIconFeedback.setImageResource(R.drawable.ic_star_white);
                        textViewFeedback.setText("Go to google play");
                        textViewShow.setText("Thank you!\n Please rate this app in Google Play");

                        linearButtonFeedback.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String url = "https://play.google.com/store/apps/details?id=com.sutech.qrcode.barcode.scanner";
                                CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();

                                CustomTabsIntent customTabsIntent = builder.build();

                                customTabsIntent.launchUrl(ScanResultActivity.this, Uri.parse(url));
                            }
                        });

                        iconBad.setImageResource(R.mipmap.ic_bad);
                        iconGood.setImageResource(R.mipmap.ic_good);
                        iconExcellent.setImageResource(R.mipmap.ic_selected_excillent);
                    }
                });

                textViewNoThanks.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });

                alertDialog.show();

            }
        });

        refreshAd();
    }

    private void checkInternetConnection() {
        CompositeDisposable disposable = new CompositeDisposable();
        disposable.add(ReactiveNetwork
                .observeNetworkConnectivity(this)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(connectivity -> {
                    if (connectivity.state() == NetworkInfo.State.CONNECTED) {
                        mBinding.flAdplaceholder.setVisibility(View.VISIBLE);
                    } else {
                        mBinding.flAdplaceholder.setVisibility(View.INVISIBLE);
                    }

                }, throwable -> {
                    Toast.makeText(this, getString(R.string.something_wrong), Toast.LENGTH_SHORT).show();
                }));
    }


    private void refreshAd() {

        AdLoader.Builder builder = new AdLoader.Builder(this, getString(R.string.admob_advand_ad_unit_id));

        builder.forUnifiedNativeAd(
                new UnifiedNativeAd.OnUnifiedNativeAdLoadedListener() {
                    // OnUnifiedNativeAdLoadedListener implementation.
                    @Override
                    public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd) {
                        // If this callback occurs after the activity is destroyed, you must call
                        // destroy and return or you may get a memory leak.
                        boolean isDestroyed = false;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                            isDestroyed = isDestroyed();
                        }
                        if (isDestroyed || isFinishing() || isChangingConfigurations()) {
                            unifiedNativeAd.destroy();
                            return;
                        }
                        // You must call destroy on old ads when you are done with them,
                        // otherwise you will have a memory leak.
                        if (nativeAd != null) {
                            nativeAd.destroy();
                        }
                        nativeAd = unifiedNativeAd;
                        FrameLayout frameLayout = findViewById(R.id.fl_adplaceholder);
                        UnifiedNativeAdView adView =
                                (UnifiedNativeAdView) getLayoutInflater()
                                        .inflate(R.layout.ad_unified, null);
                        populateUnifiedNativeAdView(unifiedNativeAd, adView);
                        frameLayout.removeAllViews();
                        frameLayout.addView(adView);
                    }
                });

        AdLoader adLoader =
                builder
                        .withAdListener(
                                new AdListener() {
                                    @Override
                                    public void onAdFailedToLoad(LoadAdError loadAdError) {
                                        String error =
                                                String.format(
                                                        "domain: %s, code: %d, message: %s",
                                                        loadAdError.getDomain(),
                                                        loadAdError.getCode(),
                                                        loadAdError.getMessage());
                                        Toast.makeText(
                                                ScanResultActivity.this,
                                                "Failed to load native ad with error " + error,
                                                Toast.LENGTH_LONG)
                                                .show();

                                        Log.e("ScanResult", error);
                                    }
                                })
                        .build();

        adLoader.loadAd(new AdRequest.Builder().build());

    }

    private void populateUnifiedNativeAdView(UnifiedNativeAd nativeAd, UnifiedNativeAdView adView) {
        // Set the media view.
        adView.setMediaView((MediaView) adView.findViewById(R.id.ad_media));

        // Set other ad assets.
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
        adView.setBodyView(adView.findViewById(R.id.ad_body));
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
        adView.setIconView(adView.findViewById(R.id.ad_app_icon));
        adView.setPriceView(adView.findViewById(R.id.ad_price));
        adView.setStarRatingView(adView.findViewById(R.id.ad_stars));
        adView.setStoreView(adView.findViewById(R.id.ad_store));
        adView.setAdvertiserView(adView.findViewById(R.id.ad_advertiser));

        // The headline and mediaContent are guaranteed to be in every UnifiedNativeAd.
        ((TextView) adView.getHeadlineView()).setText(nativeAd.getHeadline());
        adView.getMediaView().setMediaContent(nativeAd.getMediaContent());

        if (nativeAd.getBody() == null) {
            adView.getBodyView().setVisibility(View.INVISIBLE);
        } else {
            adView.getBodyView().setVisibility(View.VISIBLE);
            ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
        }

        if (nativeAd.getCallToAction() == null) {
            adView.getCallToActionView().setVisibility(View.INVISIBLE);
        } else {
            adView.getCallToActionView().setVisibility(View.VISIBLE);
            ((Button) adView.getCallToActionView()).setText(nativeAd.getCallToAction());
        }

        if (nativeAd.getIcon() == null) {
            adView.getIconView().setVisibility(View.GONE);
        } else {
            ((ImageView) adView.getIconView()).setImageDrawable(
                    nativeAd.getIcon().getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getPrice() == null) {
            adView.getPriceView().setVisibility(View.INVISIBLE);
        } else {
            adView.getPriceView().setVisibility(View.VISIBLE);
            ((TextView) adView.getPriceView()).setText(nativeAd.getPrice());
        }

        if (nativeAd.getStore() == null) {
            adView.getStoreView().setVisibility(View.INVISIBLE);
        } else {
            adView.getStoreView().setVisibility(View.VISIBLE);
            ((TextView) adView.getStoreView()).setText(nativeAd.getStore());
        }

        if (nativeAd.getStarRating() == null) {
            adView.getStarRatingView().setVisibility(View.INVISIBLE);
        } else {
            ((RatingBar) adView.getStarRatingView())
                    .setRating(nativeAd.getStarRating().floatValue());
            adView.getStarRatingView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getAdvertiser() == null) {
            adView.getAdvertiserView().setVisibility(View.INVISIBLE);
        } else {
            ((TextView) adView.getAdvertiserView()).setText(nativeAd.getAdvertiser());
            adView.getAdvertiserView().setVisibility(View.VISIBLE);
        }
        adView.setNativeAd(nativeAd);

    }


    private void setListeners() {
        mBinding.textViewOpenInBrowser.setOnClickListener(this);
        mBinding.textViewSearchInBrowser.setOnClickListener(this);
        mBinding.imageViewShare.setOnClickListener(this);
        mBinding.imageViewCopy.setOnClickListener(this);
    }

    private void loadQRCode() {
        Intent intent = getIntent();

        if (intent != null) {
            Bundle bundle = intent.getExtras();

            if (bundle != null && bundle.containsKey(IntentKey.MODEL)) {
                setCurrentCode(bundle.getParcelable(IntentKey.MODEL));
            }

            if (bundle != null && bundle.containsKey(IntentKey.IS_HISTORY)) {
                mIsHistory = bundle.getBoolean(IntentKey.IS_HISTORY);
            }

            if (bundle != null && bundle.containsKey(IntentKey.IS_PICKED_FROM_GALLERY)) {
                mIsPickedFromGallery = bundle.getBoolean(IntentKey.IS_PICKED_FROM_GALLERY);
            }
        }

        if (getCurrentCode() != null) {
            mBinding.textViewContent.setText(String.format(Locale.ENGLISH,
                    getString(R.string.content), getCurrentCode().getContent()));

            mBinding.textViewType.setText(String.format(Locale.ENGLISH, getString(R.string.code_type),
                    getResources().getStringArray(R.array.code_types)[getCurrentCode().getType()]));

            mBinding.textViewTime.setText(String.format(Locale.ENGLISH, getString(R.string.created_time),
                    TimeUtil.getFormattedDateString(getCurrentCode().getTimeStamp())));

//            mBinding.textViewOpenInBrowser.setEnabled(URLUtil.isValidUrl(getCurrentCode().getContent()));

            if (!URLUtil.isValidUrl(getCurrentCode().getContent())) {
                mBinding.textViewOpenInBrowser.setVisibility(View.GONE);
                mBinding.textViewSearchInBrowser.setVisibility(View.VISIBLE);

            }

            if (!TextUtils.isEmpty(getCurrentCode().getCodeImagePath())) {
                Glide.with(this)
                        .asBitmap()
                        .load(getCurrentCode().getCodeImagePath())
                        .into(mBinding.imageViewScannedCode);
            }

            if (SharedPrefUtil.readBooleanDefaultTrue(PreferenceKey.SAVE_HISTORY) && !mIsHistory) {
                getCompositeDisposable().add(DatabaseUtil.on().insertCode(getCurrentCode())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribeWith(new DisposableCompletableObserver() {
                            @Override
                            public void onComplete() {

                            }

                            @Override
                            public void onError(Throwable e) {

                            }
                        }));
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;

            case R.id.action_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_toolbar_menu, menu);
        setToolbarMenu(menu);
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.text_view_open_in_browser:

                String url = getCurrentCode().getContent();
                CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();

                CustomTabsIntent customTabsIntent = builder.build();

                customTabsIntent.launchUrl(this, Uri.parse(url));

                Toast.makeText(this, "Loading...", Toast.LENGTH_LONG).show();


                bundle = new Bundle();
                bundle.putString("ScanResultScr_IconOpenbrowers_Click", "Click icon Open Browers");
                firebaseAnalytics.logEvent("ScanResultScr_IconOpenbrowers_Click", bundle);

                break;

            case R.id.text_view_search_in_browser:
                String query = getCurrentCode().getContent();

                Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
                intent.putExtra(SearchManager.QUERY, query); // query contains search string
                startActivity(intent);

                bundle = new Bundle();
                bundle.putString("ScanResultScr_IconSearch_Click", "Click icon Search");
                firebaseAnalytics.logEvent("ScanResultScr_IconSearch_Click", bundle);

                break;


            case R.id.image_view_share:
                if (getCurrentCode() != null) {
                    shareCode(getCurrentCode().getContent());
                }

                bundle = new Bundle();
                bundle.putString("ScanResultScr_IconShare_Click", "Click icon Share");
                firebaseAnalytics.logEvent("ScanResultScr_IconShare_Click", bundle);
                break;

            case R.id.image_view_copy:
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);

                if (clipboard != null) {
                    ClipData clip = ClipData.newPlainText(
                            getString(R.string.scanned_qr_code_content),
                            getCurrentCode().getContent());
                    clipboard.setPrimaryClip(clip);

                    Toast.makeText(this, getString(R.string.copied_to_clipboard),
                            Toast.LENGTH_SHORT).show();
                }

                bundle = new Bundle();
                bundle.putString("ScanResultScr_IconCopy_Click", "Click icon Copy");
                firebaseAnalytics.logEvent("ScanResultScr_IconCopy_Click", bundle);
//
                break;

            default:
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getCompositeDisposable().dispose();

        if (getCurrentCode() != null
                && !SharedPrefUtil.readBooleanDefaultTrue(PreferenceKey.SAVE_HISTORY)
                && !mIsHistory && !mIsPickedFromGallery) {
            new File(getCurrentCode().getCodeImagePath()).delete();
        }
    }

    private void shareCode(String content) {
        Intent intent = new Intent(android.content.Intent.ACTION_SEND);
        String shareBody = content;
        intent.setType("text/plain");
        intent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(intent, getString(R.string.share_code_using)));
    }
}

package com.sutech.qrcode.barcode.scanner.ui.home;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.ktx.Firebase;
import com.sutech.qrcode.barcode.scanner.R;
import com.sutech.qrcode.barcode.scanner.databinding.ActivityHomeBinding;
import com.sutech.qrcode.barcode.scanner.helpers.util.PermissionUtil;
import com.sutech.qrcode.barcode.scanner.ui.generate.GenerateActivity;
import com.sutech.qrcode.barcode.scanner.ui.history.HistoryActivity;
import com.sutech.qrcode.barcode.scanner.ui.scan.ScanFragment;
import com.sutech.qrcode.barcode.scanner.ui.settings.SettingsActivity;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
public class HomeActivity extends AppCompatActivity implements View.OnClickListener {

    private FirebaseAnalytics firebaseAnalytics;
    Bundle bundle = new Bundle();

    private ActivityHomeBinding mBinding;
    private Menu mToolbarMenu;

    public Menu getToolbarMenu() {
        return mToolbarMenu;
    }

    public void setToolbarMenu(Menu toolbarMenu) {
        mToolbarMenu = toolbarMenu;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_home);

        firebaseAnalytics = FirebaseAnalytics.getInstance(this);

        getWindow().setBackgroundDrawable(null);

        setListeners();
        initializeBottomBar();
        checkInternetConnection();
        playAd();

        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        bundle = new Bundle();
        bundle.putString("MainScr_Show", "Hiện thị màn hình main");
        firebaseAnalytics.logEvent("MainScr_Show", bundle);


    }

    private void checkInternetConnection() {
        CompositeDisposable disposable = new CompositeDisposable();
        disposable.add(ReactiveNetwork
                .observeNetworkConnectivity(this)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(connectivity -> {
                    if (connectivity.state() == NetworkInfo.State.CONNECTED) {
                        mBinding.adView.setVisibility(View.VISIBLE);
                    } else {
                        mBinding.adView.setVisibility(View.INVISIBLE);
                    }

                }, throwable -> {
                    Toast.makeText(this, getString(R.string.something_wrong), Toast.LENGTH_SHORT).show();
                }));
    }

    private void playAd() {
        AdRequest adRequest = new AdRequest.Builder().build();
        mBinding.adView.loadAd(adRequest);
        mBinding.adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                mBinding.adView.setVisibility(View.INVISIBLE);
                Toast.makeText(HomeActivity.this, getString(R.string.something_wrong), Toast.LENGTH_SHORT).show();
                Log.e("Home", errorCode + " lo day");

            }


            @Override
            public void onAdOpened() {
            }

            @Override
            public void onAdLeftApplication() {
            }

            @Override
            public void onAdClosed() {
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_toolbar_menu, menu);
        setToolbarMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        int count = getSupportFragmentManager().getBackStackEntryCount();

        if (count == 0) {
            super.onBackPressed();
            //additional code
        } else {
            getSupportFragmentManager().popBackStack();
        }

    }

    private void setListeners() {
        mBinding.imageViewGenerateActive.setOnClickListener(this);
        mBinding.imageViewScanActive.setOnClickListener(this);
        mBinding.imageViewHistoryActive.setOnClickListener(this);
    }

    private void initializeBottomBar() {
        clickOnScan();
    }

    private void clickOnGenerate() {
        startActivity(new Intent(this, GenerateActivity.class));
    }

    private void clickOnScan() {
        if (PermissionUtil.on().requestPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)) {

            setToolbarTitle(getString(R.string.toolbar_title_scan));

            showFragment(ScanFragment.newInstance());
        }
    }

    private void clickOnHistory() {
        startActivity(new Intent(this, HistoryActivity.class));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.image_view_generate_active:
                Bundle bundle = new Bundle();
                bundle.putString("MainScr_IconGenerator_Click", "Click icon Generator");
                firebaseAnalytics.logEvent("MainScr_IconGenerator_Click", bundle);
                clickOnGenerate();
                break;

            case R.id.image_view_scan_active:
                bundle = new Bundle();
                bundle.putString("MainScr_IconScan_Click", "Click icon Scan");
                firebaseAnalytics.logEvent("MainScr_IconScan_Click", bundle);
                clickOnScan();
                break;

            case R.id.image_view_history_active:
                bundle = new Bundle();
                bundle.putString("MainScr_IconHistory_Click", "Click icon History");
                firebaseAnalytics.logEvent("MainScr_IconHistory_Click", bundle);
                clickOnHistory();
                break;
        }
    }

    private void setToolbarTitle(String title) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }
    }

    private void showFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.coordinator_layout_fragment_container, fragment,
                fragment.getClass().getSimpleName());
        transaction.commit();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == PermissionUtil.REQUEST_CODE_PERMISSION_DEFAULT) {
            boolean isAllowed = true;

            for (int i = 0; i < permissions.length; i++) {
                if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                    isAllowed = false;
                }
            }

            if (isAllowed) {
                clickOnScan();
            }else{
                Toast.makeText(this, "Please allow using the camera and accessing the storage", Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }

  /*  public void hideAdMob()
    {
        if (mBinding.adView.isShown())
            mBinding.adView.setVisibility(View.GONE);
    }

    public void showAdmob()
    {
        if (!mBinding.adView.isShown())
            mBinding.adView.setVisibility(View.VISIBLE);
    }*/
}

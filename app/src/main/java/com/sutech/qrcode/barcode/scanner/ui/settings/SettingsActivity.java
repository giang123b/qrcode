package com.sutech.qrcode.barcode.scanner.ui.settings;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.databinding.DataBindingUtil;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.sutech.qrcode.barcode.scanner.R;
import com.sutech.qrcode.barcode.scanner.databinding.ActivitySettingsBinding;
import com.sutech.qrcode.barcode.scanner.helpers.constant.PreferenceKey;
import com.sutech.qrcode.barcode.scanner.helpers.util.SharedPrefUtil;
import com.sutech.qrcode.barcode.scanner.ui.about_us.AboutUsActivity;
import com.sutech.qrcode.barcode.scanner.ui.privacy_policy.PrivayPolicyActivity;

public class SettingsActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {

    private ActivitySettingsBinding mBinding;

    private FirebaseAnalytics firebaseAnalytics;
    Bundle bundle = new Bundle();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_settings);

        initializeToolbar();
        loadSettings();
        setListeners();

        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        bundle = new Bundle();
        bundle.putString("SettingScr_Show", "Hiển thị màn hình Setting");
        firebaseAnalytics.logEvent("SettingScr_Show", bundle);
    }

    private void loadSettings() {
        mBinding.switchCompatPlaySound.setChecked(SharedPrefUtil.readBooleanDefaultTrue(PreferenceKey.PLAY_SOUND));
        mBinding.switchCompatVibrate.setChecked(SharedPrefUtil.readBooleanDefaultTrue(PreferenceKey.VIBRATE));
    }

    private void setListeners() {
        mBinding.switchCompatPlaySound.setOnCheckedChangeListener(this);
        mBinding.switchCompatVibrate.setOnCheckedChangeListener(this);
        mBinding.textViewPlaySound.setOnClickListener(this);
        mBinding.textViewVibrate.setOnClickListener(this);

        mBinding.textViewRate.setOnClickListener(this);
        mBinding.textViewFeedback.setOnClickListener(this);
        mBinding.textViewPrivacyPolicy.setOnClickListener(this);

    }

    private void initializeToolbar() {
        setSupportActionBar(mBinding.toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.switch_compat_play_sound:
                SharedPrefUtil.write(PreferenceKey.PLAY_SOUND, isChecked);

                if (isChecked){
                    bundle = new Bundle();
                    bundle.putString("SettingScr_Iconnsound_ClickOff", "Click icon Scan Sound");
                    firebaseAnalytics.logEvent("SettingScr_Iconnsound_ClickOff", bundle);
                }else {

                    bundle = new Bundle();
                    bundle.putString("SettingScr_Iconnsound_ClickOn", "Click icon Scan Sound");
                    firebaseAnalytics.logEvent("SettingScr_Iconnsound_ClickOn", bundle);
                }

                break;

            case R.id.switch_compat_vibrate:
                SharedPrefUtil.write(PreferenceKey.VIBRATE, isChecked);

                if (isChecked){
                    bundle = new Bundle();
                    bundle.putString("SettingScr_IconVibrate_ClickOff", "Click icon Vibrate");
                    firebaseAnalytics.logEvent("SettingScr_IconVibrate_ClickOff", bundle);
                }else {

                    bundle = new Bundle();
                    bundle.putString("SettingScr_IconVibrate_ClickOn", "Click icon Vibrate");
                    firebaseAnalytics.logEvent("SettingScr_IconVibrate_ClickOn", bundle);
                }

                break;

            default:
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.text_view_play_sound:
                mBinding.switchCompatPlaySound.setChecked(!mBinding.switchCompatPlaySound.isChecked());
                break;

            case R.id.text_view_vibrate:
                mBinding.switchCompatVibrate.setChecked(!mBinding.switchCompatVibrate.isChecked());
                break;

            case R.id.text_view_rate:
                String url = "https://play.google.com/store/apps/details?id=com.sutech.qrcode.barcode.scanner";
                CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();

                CustomTabsIntent customTabsIntent = builder.build();

                customTabsIntent.launchUrl(this, Uri.parse(url));

                bundle = new Bundle();
                bundle.putString("SettingScr_IconRateApp_Click", "Click icon Rate Application");
                firebaseAnalytics.logEvent("SettingScr_IconRateApp_Click", bundle);

                break;

            case R.id.text_view_feedback:

                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.putExtra(Intent.EXTRA_SUBJECT, "Feedback App QR & Barcode Scanner");
                intent.putExtra(Intent.EXTRA_TEXT, "What is your problem?");
                intent.setData(Uri.parse("mailto:sutechmobile@gmail.com")); // or just "mailto:" for blank
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
                startActivity(intent);

                bundle = new Bundle();
                bundle.putString("SettingScr_IconFeedback_Click", "Click icon Feedback");
                firebaseAnalytics.logEvent("SettingScr_IconFeedback_Click", bundle);

                break;

            case R.id.text_view_privacy_policy:
                url = "https://sutechmobile.blogspot.com/2021/06/qr-code-privacy-policy.html";
                builder = new CustomTabsIntent.Builder();
                customTabsIntent = builder.build();
                customTabsIntent.launchUrl(this, Uri.parse(url));

                bundle = new Bundle();
                bundle.putString("SettingScr_IconPolicy_Click", "Click icon Policy");
                firebaseAnalytics.logEvent("SettingScr_IconPolicy_Click", bundle);

                break;

            default:
                break;
        }
    }

    public void startAboutUsActivity(View view) {

        startActivity(new Intent(this, AboutUsActivity.class));
    }

    public void startPrivacyPolicyActivity(View view) {
        startActivity(new Intent(this, PrivayPolicyActivity.class));
    }
}

package com.sutech.qrcode.barcode.scanner.ui.splash;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.sutech.qrcode.barcode.scanner.R;
import com.sutech.qrcode.barcode.scanner.ui.home.HomeActivity;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;


public class SplashActivity extends AppCompatActivity {

    /**
     * Constants
     */
    private final int SPLASH_DELAY = 2000;

    /**
     * Fields
     */
    private ImageView mImageViewLogo;

    private FirebaseAnalytics firebaseAnalytics;

    Bundle bundle = new Bundle();

    private InterstitialAd interstitialAd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        firebaseAnalytics = FirebaseAnalytics.getInstance(this);

        getWindow().setBackgroundDrawable(null);

        initializeViews();
        animateLogo();

        bundle = new Bundle();
        bundle.putString("SplashScr_Show", "Hiện thị màn hình splash");
        firebaseAnalytics.logEvent("SplashScr_Show", bundle);

        mImageViewLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bundle = new Bundle();
                bundle.putString("AppStart_Click", "Click vào icon laucher");
                firebaseAnalytics.logEvent("AppStart_Click", bundle);
            }
        });

        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });

        // Create the InterstitialAd and set the adUnitId.
        interstitialAd = new InterstitialAd(this);
        // Defined in res/values/strings.xml
        interstitialAd.setAdUnitId(getString(R.string.admob_interstitial_ad_unit_id_test));

        interstitialAd.setAdListener(
                new AdListener() {
                    @Override
                    public void onAdLoaded() {
                    }

                    @Override
                    public void onAdFailedToLoad(LoadAdError loadAdError) {
                        Log.e("Splash", loadAdError.toString());
                        startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                        finish();
                    }

                    @Override
                    public void onAdClosed() {
                        startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                        finish();
                    }
                });

        if (interstitialAd != null && interstitialAd.isLoaded()) {
            interstitialAd.show();
        }else{
            if (!interstitialAd.isLoading() && !interstitialAd.isLoaded()) {
                AdRequest adRequest = new AdRequest.Builder().build();
                interstitialAd.loadAd(adRequest);
            }
        }


        goToMainPage();

//        checkInternetConnection();

    }

//    private void checkInternetConnection() {
//        CompositeDisposable disposable = new CompositeDisposable();
//        disposable.add(ReactiveNetwork
//                .observeNetworkConnectivity(this)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(connectivity -> {
//                    if (connectivity.state() == NetworkInfo.State.CONNECTED) {
//                        if (interstitialAd != null && interstitialAd.isLoaded()) {
//                            interstitialAd.show();
//                        } else {
//                            if (!interstitialAd.isLoading() && !interstitialAd.isLoaded()) {
//                                AdRequest adRequest = new AdRequest.Builder().build();
//                                interstitialAd.loadAd(adRequest);
//                            }
//                        }
//                    }
//
//                }, throwable -> {
//                    Toast.makeText(this, getString(R.string.something_wrong), Toast.LENGTH_SHORT).show();
//                }));
//    }



    @Override
    public void onResume() {
        super.onResume();
    }


    /**
     * This method initializes the views
     */
    private void initializeViews() {
        mImageViewLogo = findViewById(R.id.image_view_logo);
    }

    /**
     * This method takes user to the main page
     */
    private void goToMainPage() {
        if (interstitialAd != null && interstitialAd.isLoaded()) {
            interstitialAd.show();
        }else{
            if (!interstitialAd.isLoading() && !interstitialAd.isLoaded()) {
                AdRequest adRequest = new AdRequest.Builder().build();
                interstitialAd.loadAd(adRequest);
            }
        }

        new Handler().postDelayed(() -> {
            if (interstitialAd != null && interstitialAd.isLoaded()) {
                interstitialAd.show();
            } else {
                if (!interstitialAd.isLoading() && !interstitialAd.isLoaded()) {
                    AdRequest adRequest = new AdRequest.Builder().build();
                    interstitialAd.loadAd(adRequest);
                }

                startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                finish();
            }

        }, 6000);
    }

    /**
     * This method animates the logo
     */
    private void animateLogo() {
        Animation fadeInAnimation = AnimationUtils.loadAnimation(this, R.anim.fade_in_without_duration);
        fadeInAnimation.setDuration(SPLASH_DELAY);

        mImageViewLogo.startAnimation(fadeInAnimation);
    }
}
